Digital Sathyavedapusthakam
===========================

This is the content of The Holy Bible translation into Malayalam
language known as `Sathyavedapusthakam published by The Bible Society
of India in 1910 <http://ml.wikipedia.org/wiki/സത്യവേദപുസ്തകം>`_.  The
source format used here is `USFM <http://paratext.org/about/usfm>`_.
The text can be converted to other formats using USFM converters.

* `Project Website <http://sathyavedapusthakam.in>`_
* `Discussion Group <https://groups.google.com/d/forum/sathyavedapusthakam>`_

Installation
------------

To run the *svpm* program, you need Python 3.4 and few other third
party dependencies.  This is only tested in Debian GNU/Linux -
*Jessie* version and Fedora 20.  It may work in other GNU/Linux
distributions also.  These are the steps to install *svpm* program in
Debian GNU/Linux and Fedora 20.

1. Install `development libraries
   <https://docs.python.org/devguide/setup.html>`_ and other
   dependencies as root user

   a. Debian

      ::

        # apt-get -y build-dep python3
        # apt-get -y install wget git

   b. Fedora

      ::

        # yum install -y yum-utils wget git
        # yum-builddep python3

2. Install Python 3.4

   a. Download `Python 3.4 XZ compressed source tarball
      <https://www.python.org/ftp/python/3.4.0/Python-3.4.0.tar.xz>`_

      ::

      
        $ wget -c https://www.python.org/ftp/python/3.4.0/Python-3.4.0.tar.xz

   b. Extract Python source tarball, build and install

      ::

        $ tar Jxvf Python-3.4.0.tar.xz
        $ cd Python-3.4.0
        $ ./configure --prefix=$HOME/usr
        $ make
        $ make install

3. Create a `virtual environment
   <https://docs.python.org/3.4/library/venv.html>`_ and activate it

   ::

     $ $HOME/usr/bin/pyvenv $HOME/ve
     $ source $HOME/ve/bin/activate

4. Clone the `parsely <http://parsley.readthedocs.org>`_ code from the
   Git repository and install it

   ::

     $ cd $HOME
     $ git clone git@github.com:vsajip/parsley.git
     $ cd parsley
     $ python setup.py install

5. Clone the *svpm* code from the Git repository and install it

   ::

     $ cd $HOME
     $ git clone git@bitbucket.org:sathyavedapusthakam/svpm.git
     $ cd svpm
     $ python setup.py develop

6. Now you can run the *svpm* program with various arguments
   For example, to convert the USFM files to HTML::

     $ svpm -c conf/html.cfg --html


Proofreading
------------

The USFM source text may contain spelling mistakes.  You are welcome
to proofread the text and report the issues.  More details about this
work is given in the `wiki page
<https://bitbucket.org/sathyavedapusthakam/svpm/wiki/Proofreading>`_
(Malayalam language).


Attribution
-----------

The Unicode text was taken from `Malayalam Wikisource
<http://ml.wikisource.org/wiki/സത്യവേദപുസ്തകം>`_ which is released under
Creative Commons `Attribution-ShareAlike 3.0 Unported
<http://creativecommons.org/licenses/by-sa/3.0/>`_ License.  The text
was digitally encoded and contributed to Malayalam Wikisource by
`Nishad Kaippally <http://malayalambible.in>`_ and released under
Creative Commons `Attribution-ShareAlike 4.0 International
<http://creativecommons.org/licenses/by-sa/4.0/>`_ License.
